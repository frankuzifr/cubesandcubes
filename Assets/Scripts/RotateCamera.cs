﻿using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    public float Speed = 5f;
    private Transform _rotator;
    
    private void Start()
    {
        _rotator = GetComponent<Transform>();
    }

    private void Update()
    {
        _rotator.Rotate(0, Speed * Time.deltaTime, 0);
    }
}
