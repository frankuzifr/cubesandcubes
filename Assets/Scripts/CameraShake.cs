﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private Transform camTransform;
    private float shakeeDur = 1f, shakeAmout = 0.04f, decreaseFactor = 1.5f;

    private Vector3 originPos;

    private void Start()
    {
        camTransform = GetComponent<Transform>();
        originPos = camTransform.localPosition;
    }

    private void Update()
    {
        if (shakeeDur > 0)
        {
            camTransform.localPosition = originPos + Random.insideUnitSphere * shakeAmout;
            shakeeDur -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeeDur = 0;
            camTransform.localPosition = originPos;
        }
    }
}
